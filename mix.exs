defmodule ElementTui.MixProject do
  use Mix.Project

  def project do
    [
      app: :elementtui,
      version: "0.2.1",
      elixir: "~> 1.16",
      compilers: [:elixir_make] ++ Mix.compilers(),
      make_targets: ["all"],
      make_clean: ["clean"],
      start_permanent: Mix.env() == :prod,
      name: "ElementTui",
      description: "Library to create a terminal user interface, based on termbox2.",
      package: package(),
      deps: deps(),
      source_url: "https://gitlab.com/BlackEdder/elementtui"
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {ElementTui.Application, []}
    ]
  end

  defp deps do
    [
      {:mix_test_watch, "~> 1.0", only: :dev, runtime: false},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:elixir_make, "~> 0.8", runtime: false},
      {:briefly, "~> 0.5"}
    ]
  end

  defp package do
    [
      name: "elementtui",
      files: ~w(lib .formatter.exs mix.exs README.md LICENSE src Makefile),
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/BlackEdder/elementtui",
        "examples" => "https://gitlab.com/BlackEdder/elementtui_examples"
      }
    ]
  end
end
