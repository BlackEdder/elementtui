# ElementTui

Library to create a terminal user interface, building on termbox2. Current functionality was build to support rtttex (https://gitlab.com/BlackEdder/rttt-ex), but I am happy with the base functionality and open to merge requests to improve and expand it.

## Installation

The package can be installed by adding `elementtui` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:elementtui, "~> 0.2.1"}
  ]
end
```

## Usage

There are some simple examples on [gitlab](https://gitlab.com/BlackEdder/elementtui_examples) that should help you get started. Note that when starting your own project you need to make sure elixir/erlang does not listen for input, by setting [-noinput](https://gitlab.com/BlackEdder/elementtui_examples/-/blame/main/mix.exs?ref_type=heads#L14) in the mix.exs file.

## Examples

For the code to create these examples see [BlackEdder/elementtui_examples](https://gitlab.com/BlackEdder/elementtui_examples)

### Popup

![popup](https://gitlab.com/BlackEdder/elementtui/-/wikis/uploads/c95e9becc78aff37ec867ad64925082d/popup.gif)
