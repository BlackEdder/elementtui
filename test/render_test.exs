defmodule RenderTest do
  use ExUnit.Case, async: false

  alias ElementTui.Element
  alias ElementTui.Parser

  @ipsum "We started an agent with an initial state of an empty list. We updated the agent's state, adding our new item to the head of the list. The second argument of Agent.update/3 is a function that takes the agent's current state as input and returns its desired new state. Finally, we retrieved the whole list. The second argument of Agent.get/3 is a function that takes the state as input and returns the value that Agent.get/3 itself will return. Once we are done with the agent, we can call Agent.stop/3 to terminate the agent process."

  @ipsum_short "We started an agent with an initial state of an empty list. We updated the agent's state, adding our new item to the head of the list."

  setup_all do
    # TODO: Find a clean way for this to happen automatically
    ElementTui.Tui.terminate("We need to run a test", nil)
    :ok
  end

  setup %{file: file, test: test, module: module} do
    filename = to_string(module) <> to_string(test)
    dir = Path.dirname(file) |> Path.join("golden")
    File.mkdir(dir)
    filename = dir |> Path.join(filename)

    {:ok, golden: filename}
  end

  test "unfold basic text box", %{golden: golden} do
    el =
      @ipsum
      |> Element.text()
      |> Element.wrap()

    lns = el |> Parser.parse(0, 0, 30, 5)

    assert Enum.count(lns.elements) == 5
    assert Parser.calculate_dim(el, 30, 5) == {30, 5}

    gold(lns, golden, "1")

    # Attributes are "transmitted" to all elements
    lns =
      @ipsum
      |> Element.text()
      |> Element.bold()
      |> Element.wrap()
      |> Parser.parse(0, 0, 30, 30)

    gold(lns, golden, "2")

    lns =
      @ipsum
      |> Element.text()
      |> Parser.parse(0, 0, 30, 30)

    gold(lns, golden, "3")
  end

  test "parse an element using on_height_exceeded", %{golden: golden} do
    lns =
      Element.text(@ipsum)
      |> Element.wrap()
      |> Element.height(4, fn el, _height ->
        s = Element.text("Line 1")
        e = Element.text("Last line")
        [s, el |> Element.height(2), e] |> Element.vbox()
      end)
      |> Parser.parse(0, 0, 30, 30)

    gold(lns, golden, "1")
  end

  test "We can handle line enters", %{golden: golden} do
    lns = "very short\nvery short\n" |> Element.text() |> Parser.parse(0, 0, 30, 100)
    gold(lns, golden, "1")

    ln = @ipsum_short <> "\nvery short\nvery short\n" <> @ipsum_short
    lns = ln |> Element.text() |> Element.wrap() |> Parser.parse(0, 0, 30, 100)
    gold(lns, golden, "2")
  end

  test "support layers", %{golden: golden} do
    lns =
      String.duplicate("background ", 10)
      |> Element.text()
      |> Element.wrap()

    gold(lns, golden, "1")

    # Attributes are "transmitted" to all elements
    lns2 =
      String.duplicate("middle ", 10)
      |> Element.text()
      |> Element.wrap()

    gold(lns2, golden, "2")

    lns3 =
      String.duplicate("foreground ", 10)
      |> Element.text()
      |> Element.wrap()

    gold(lns3, golden, "3")

    parse =
      [lns, lns2, lns3]
      # Ensures they are parsed in the correct order
      |> Element.layerbox()
      |> Parser.parse(0, 0, 30, 100)

    gold(parse, golden, "4")
  end

  test "parse a vbox with multiple elements", %{golden: golden} do
    vb =
      [
        @ipsum
        |> Element.text()
        |> Element.wrap(),
        @ipsum
        |> Element.text()
        |> Element.bold()
        |> Element.wrap()
      ]
      |> Element.vbox()

    lns = Parser.parse(vb, 0, 0, 30, 30)
    gold(lns, golden, "1")

    vb =
      [
        @ipsum
        |> Element.text()
        |> Element.wrap()
        |> Element.height(5),
        @ipsum
        |> Element.text()
        |> Element.bold()
        |> Element.wrap()
      ]
      |> Element.vbox()

    lns = Parser.parse(vb, 0, 0, 30, 30)
    gold(lns, golden, "2")
  end

  test "parse a hbox", %{golden: golden} do
    hb =
      [
        Element.indent(2),
        @ipsum
        |> Element.text()
        |> Element.wrap()
      ]
      |> Element.hbox()

    lns = Parser.parse(hb, 0, 0, 30, 30)
    gold(lns, golden, "1")
  end

  test "parse a hbox with vfill", %{golden: golden} do
    hb =
      [
        Element.text("┃")
        |> Element.width(1)
        |> Element.vfill(),
        @ipsum
        |> Element.text()
        |> Element.wrap()
      ]
      |> Element.hbox()

    lns = Parser.parse(hb, 0, 0, 30, 30)
    gold(lns, golden, "1")
  end

  test "scroll a vbox", %{golden: golden} do
    es =
      0..30
      |> Enum.map(fn i ->
        case i == 12 do
          false ->
            Element.text("#{i} No focus for me")

          true ->
            Element.text("#{i} I am in focus!") |> Element.focus()
        end
      end)
      |> Element.vbox(scroll: true)

    p = Parser.parse(es, 0, 0, 30, 9)
    gold(p, golden, "1")

    # Test edge cases (near top and near bottom)
    es =
      0..30
      |> Enum.map(fn i ->
        case i == 2 do
          false ->
            Element.text("#{i} No focus for me")

          true ->
            Element.text("#{i} I am in focus!") |> Element.focus()
        end
      end)
      |> Element.vbox(scroll: true)

    p = Parser.parse(es, 0, 0, 30, 9)
    gold(p, golden, "2")

    es =
      0..30
      |> Enum.map(fn i ->
        case i == 28 do
          false ->
            Element.text("#{i} No focus for me")

          true ->
            Element.text("#{i} I am in focus!") |> Element.focus()
        end
      end)
      |> Element.vbox(scroll: true)

    p = Parser.parse(es, 0, 0, 30, 9)
    gold(p, golden, "3")

    # None are in focus
    es =
      0..30
      |> Enum.map(fn i ->
        case i == 100 do
          false ->
            Element.text("#{i} No focus for me")

          true ->
            Element.text("#{i} I am in focus!") |> Element.focus()
        end
      end)
      |> Element.vbox(scroll: true)

    p = Parser.parse(es, 0, 0, 30, 9)
    gold(p, golden, "4")
  end

  test "add border and center a text box", %{golden: golden} do
    ln = "Hello from the popup"

    popup =
      Element.text(ln)
      |> Element.width(String.length(ln))
      |> Element.border()
      |> Element.center()

    p = Parser.parse(popup, 0, 0, 40, 12)

    gold(p, golden, "1")
    assert p.width == String.length(ln) + 2
  end

  #
  # Golden test helper functions
  #

  defp gold(result, filename, lbl) do
    filename = filename <> lbl <> ".golden"

    case File.exists?(filename) do
      false ->
        IO.puts("First time running the test, saving output.")
        IO.puts("Current Output: #{inspect(result)}")
        save_reference(filename, result)

      true ->
        reference = load_reference(filename)

        if result != reference do
          IO.puts("Test failed. Current output differs from the reference.")
          IO.puts("Current Output: #{inspect(result)}")
          IO.puts("Reference Output: #{inspect(reference)}")

          # Ask the user if they want to accept the changes
          answer = IO.gets("Do you want to accept the changes? (y/n) ")

          if answer == "y\n" do
            # If accepted, update the reference file
            save_reference(filename, result)
            assert true
          else
            assert result == reference
          end
        end
    end
  end

  defp load_reference(reference_file) do
    File.read!(reference_file) |> :erlang.binary_to_term()
  end

  defp save_reference(reference_file, result) do
    File.write!(reference_file, result |> :erlang.term_to_binary())
  end
end
