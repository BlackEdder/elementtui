defmodule ElementTui.Element do
  @moduledoc """
  Elements are the core of the TUI layout. Importantly they are composable so that you can build complex layouts from simple elements.

  ```
  # Create a text element with width 10
  Element.text("Hello") |> Element.width(10)
  ```

  """

  # Creating elements
  def blank(width) do
    line = String.duplicate(" ", width)
    %{line: line, attr: 0, properties: [{:width, width}]}
  end

  def indent(width) do
    %{line: "", attr: 0, properties: [{:width, width}]}
  end

  def text(text) do
    %{text: text, attr: 0, properties: []}
  end

  # Adapting elements/properties
  def border(element) do
    element |> add_property(:border)
  end

  def center(element) do
    element |> add_property(:center)
  end

  def width(element, width) do
    element |> add_property({:width, width})
  end

  def height(element, height, on_height_exceeded \\ fn x, _ -> x end) do
    element |> add_property({:height, height, on_height_exceeded})
  end

  def hbox(elements) do
    %{properties: [{:hbox, elements}]}
  end

  @doc """
  Plot multiple layers on top of each other. Ensures that the layers are drawn in the correct order.
  """
  def layerbox(elements) when is_list(elements) do
    %{properties: [{:layerbox, elements}]}
  end

  @doc """
  Opts can contain scroll: true
  """
  def vbox(elements, opts \\ []) do
    %{properties: [{:vbox, elements, opts}]}
  end

  def vfill(element) do
    element |> add_property(:vfill)
  end

  # Text attributes?!?
  def focus(element) do
    Map.put(element, :focus, true)
  end

  def wrap(element) do
    Map.put(element, :wrap, true)
  end

  def bold(element) do
    %{element | attr: Bitwise.bor(element.attr, 0x0100)}
  end

  def dim(element) do
    %{element | attr: Bitwise.bor(element.attr, 0x8000)}
  end

  def invert_colour(element) do
    %{element | attr: Bitwise.bor(element.attr, 0x0400)}
  end

  def render(elements) when is_list(elements) do
    elements
    |> Enum.each(fn {el} ->
      ElementTui.Tui.puts(el.x, el.y, el.attr, el.line)
    end)

    elements
  end

  def render(element) do
    ElementTui.Tui.puts(element.x, element.y, element.attr, element.line)
  end

  defp add_property(element, property) do
    %{element | properties: [property | element.properties]}
  end
end
