defmodule ElementTui.Parser do
  alias ElementTui.Element

  # TODO: Specialise calculate_dim for more performance
  # Can we actually specialise much without carrying more info, i.e if :width is set, we still have to keep parsing
  # to know the height, and by the time we find :height, we don't know if we can just return that either
  def calculate_dim(%{properties: [:vfill | tl]} = element, width, height) do
    # Return the minimum height needed, if we don't replicate it
    el = element |> Map.put(:properties, tl)
    calculate_dim(el, width, height)
  end

  def calculate_dim(%{width: w, height: h}, _width, _height) do
    {w, h}
  end

  def calculate_dim(element, width, height) do
    %{width: w, height: h} = parse(element, 0, 0, width, height)
    {w, h}
  end

  def parse(%{properties: [:vfill | tl]} = element, x, y, width, height) do
    el = element |> Map.put(:properties, tl)
    {_, h} = calculate_dim(el, width, height)

    n = div(height, h)
    left_over_height = rem(h, height)

    # Use left over height to plot a partial one if needed
    el =
      if left_over_height > 0 do
        [el |> Element.height(left_over_height) | List.duplicate(el, n)]
        |> Enum.reverse()
        |> Element.vbox()
      else
        List.duplicate(el, n) |> Element.vbox()
      end

    parse(el, x, y, width, height)
  end

  def parse(%{properties: [{:layerbox, xs}]}, x, y, width, height) do
    [h | tl] = Enum.reverse(xs)
    acc = parse(h, x, y, width, height)

    tl
    |> Enum.reduce(acc, fn el, acc ->
      %{elements: elements, height: h, width: w} = parse(el, x, y, width, height)
      %{elements: elements ++ acc.elements, height: max(h, acc.height), width: max(w, acc.width)}
    end)
  end

  def parse(%{properties: [{:vbox, xs, [scroll: true]}]}, x, y, width, height) do
    # Split before focus window (or at the end)
    {pre, post} =
      xs
      |> Enum.split_while(fn
        %{focus: true} -> false
        _ -> true
      end)

    case post do
      [] ->
        # No focus found, start at the top
        xs |> Element.vbox() |> parse(x, y, width, height)

      _ ->
        {xs, ys} = focus_helper(Enum.reverse(pre), post, width, height)

        (Enum.reverse(xs) ++ ys)
        |> Element.vbox()
        |> parse(x, y, width, height)
    end
  end

  def parse(%{properties: [{:vbox, xs, _}]}, x, y, width, height) do
    # Parse a vertical box
    {elements, acc_height, acc_width} =
      xs
      |> Enum.reduce_while({[], 0, 0}, fn el, {es, acc_height, acc_width} = acc ->
        case height - acc_height do
          next_height when next_height <= 0 ->
            {:halt, acc}

          next_height ->
            %{elements: elements, height: h, width: w} =
              parse(el, x, y + acc_height, width, next_height)

            {:cont, {es ++ elements, acc_height + h, max(w, acc_width)}}
        end
      end)

    %{elements: elements, height: acc_height, width: acc_width}
  end

  def parse(%{properties: [{:hbox, xs}]}, x, y, width, height) do
    # Get the max height with the calculate_dim
    {_, height} =
      Enum.reduce_while(xs, {0, 1}, fn el, {acc_w, acc_h} = acc ->
        case width - acc_w do
          next_width when next_width <= 0 ->
            {:halt, acc}

          next_width ->
            {w, h} = calculate_dim(el, next_width, height)
            {:cont, {w + acc_w, max(h, acc_h)}}
        end
      end)

    {elements, acc_width} =
      xs
      |> Enum.reduce_while({[], 0}, fn el, {es, acc_width} = acc ->
        case width - acc_width do
          next_width when next_width <= 0 ->
            {:halt, acc}

          next_width ->
            %{elements: elements, width: w} =
              parse(el, x + acc_width, y, next_width, height)

            {:cont, {es ++ elements, acc_width + w}}
        end
      end)

    %{elements: elements, height: height, width: acc_width}
  end

  def parse(%{properties: [:center | tl]} = element, x, y, width, height) do
    el = element |> Map.put(:properties, tl)
    # TODO: Could we do this without double parsing?
    {w, h} = calculate_dim(el, width, height)
    x = div(width, 2) + x - div(w, 2)
    y = div(height, 2) + y - div(h, 2)
    parse(el, x, y, w, h)
  end

  def parse(%{properties: [:border | tl]} = element, x, y, width, height) do
    # TODO: Move border elements to constants module
    # https://en.wikipedia.org/wiki/Box-drawing_characters
    bordered_el = element |> Map.put(:properties, tl)
    {w, h} = calculate_dim(bordered_el, width - 2, height - 2)
    hline = "─"
    vline = "│"
    tl = Element.text("┌") |> Element.width(1)
    tr = Element.text("┐") |> Element.width(1)
    bl = Element.text("└") |> Element.width(1)
    br = Element.text("┘") |> Element.width(1)
    # border = String.duplicate(hline, w)
    # top_border = tl <> border <> tr
    # bottom_border = bl <> border <> br
    # Something goes wrong with String duplicate and these utf8 characters, so ...
    border = List.duplicate(Element.text(hline) |> Element.width(1), w)
    top_border = [tl | border ++ [tr]] |> Element.hbox()
    bottom_border = [bl | border ++ [br]] |> Element.hbox()

    # TODO: Get an element indent that does this? Would use replicate_y?
    # Might fail due to UTF8 oddities
    el = List.duplicate(Element.text(vline) |> Element.width(1), h) |> Element.vbox()

    el =
      [
        el,
        bordered_el,
        el
      ]
      |> Element.hbox()

    el =
      [
        top_border,
        el,
        bottom_border
      ]
      |> Element.vbox()

    parse(el, x, y, w + 2, h + 2)
  end

  def parse(%{properties: [{:height, mx, func} | tl]} = element, x, y, width, height) do
    # Do these checks on max_height, and don't worry about height, because if height is less that probably
    # means we are at the bottom of the screen.
    # Parse element, see if it fits in the height provided
    el = element |> Map.put(:properties, tl)
    {_, h} = calculate_dim(el, width, mx + 1)

    element =
      case h > mx do
        # If does not fit then call the height function. The height function should return a new element that fits
        # We don't check that again
        true ->
          func.(el, mx)

        false ->
          el
      end

    parse(
      element,
      x,
      y,
      width,
      min(height, mx)
    )
  end

  def parse(%{properties: [{:width, mx} | tl]} = element, x, y, width, height) do
    el = element |> Map.put(:properties, tl)
    parse(el, x, y, min(width, mx), height)
  end

  def parse(%{line: line} = element, x, y, width, _height) do
    el =
      element
      |> Map.put(:x, x)
      |> Map.put(:y, y)
      |> Map.put(:width, width)
      |> Map.put(:height, 1)
      |> Map.put(:line, String.pad_trailing(line, width))

    %{elements: [el], height: 1, width: width}
  end

  def parse(%{text: text, wrap: true} = element, x, y, width, height) do
    # TODO: Should wrap_text correctly handle \n in the text

    text
    |> String.split("\n")
    |> Enum.flat_map(fn ln ->
      wrap_text(ln, width)
    end)
    |> Enum.map(fn line ->
      element
      |> Map.delete(:wrap)
      |> Map.put(:text, line)
    end)
    |> Element.vbox()
    |> parse(x, y, width, height)
  end

  def parse(%{text: text} = element, x, y, width, _height) do
    # Parse a text element
    line =
      text
      |> String.split("\n")
      |> Kernel.hd()
      |> String.slice(0, width)
      |> String.pad_trailing(width)

    el =
      element
      |> Map.delete(:text)
      |> Map.put(:line, line)
      |> Map.put(:x, x)
      |> Map.put(:y, y)
      |> Map.put(:width, width)
      |> Map.put(:height, 1)

    %{elements: [el], height: 1, width: width}
  end

  defp wrap_text(text, max_length) when is_binary(text) do
    text
    |> String.split(~r(\s), trim: true)
    |> Enum.reduce({[""], 0}, fn word, {[h | tl], l} ->
      ln = String.length(word)

      case h do
        "" ->
          {[word | tl], ln}

        _ ->
          if l + ln + 1 <= max_length do
            {[h <> " " <> word | tl], l + ln + 1}
          else
            {[word, h | tl], ln}
          end
      end
    end)
    |> elem(0)
    |> Enum.reverse()
  end

  defp focus_helper([], [], _width, _available_height) do
    {[], []}
  end

  defp focus_helper(rpre, [], width, available_height) do
    # Take while acc_height > 0. Note that this has the cost of always picking a whole item at the top
    # Not sure how to fix that easily though. Future maybe pass an offset back, to only start at ...

    # - 4 used, to allow for a few empty lines at the end and signify that the end was reached
    xs =
      Stream.transform(rpre, available_height - 4, fn el, acc ->
        %{height: height} = parse(el, 0, 0, width, acc)

        if height >= acc do
          {:halt, acc - height}
        else
          {[el], acc - height}
        end
      end)
      |> Enum.to_list()

    {xs, []}
  end

  defp focus_helper([], post, width, available_height) do
    # Take while acc_height > 0.
    xs =
      Stream.transform(post, available_height, fn el, acc ->
        if acc < 0 do
          {:halt, acc}
        else
          %{height: height} = parse(el, 0, 0, width, acc)
          {[el], acc - height}
        end
      end)
      |> Enum.to_list()

    {[], xs}
  end

  defp focus_helper(
         [rpre_head | rpre_tail],
         [post_head | post_tail],
         width,
         available_height
       ) do
    # Post is first, because the focussed element will be in there
    %{height: post_h} = parse(post_head, 0, 0, width, available_height)
    available_height = available_height - post_h

    case available_height > 0 do
      true ->
        %{height: pre_h} = parse(rpre_head, 0, 0, width, available_height)
        available_height = available_height - pre_h

        case available_height > 0 do
          true ->
            # If still left over, call focus_helper again
            {xs, ys} =
              focus_helper(
                rpre_tail,
                post_tail,
                width,
                available_height
              )

            {[rpre_head | xs], [post_head | ys]}

          false ->
            {[rpre_head], [post_head]}
        end

      false ->
        {[], [post_head]}
    end
  end
end
