defmodule ElementTui.Application do
  @moduledoc """
  Opens a window (tb_init) and kills it on shutdown (tb_shutdown)
  """
  use Application

  @impl true
  def start(_type, _args) do
    ElementTui.TermBox2Ex.load_nifs()

    children =
      [
        {ElementTui.Tui, name: ElementTui.Tui}
      ]

    opts = [strategy: :one_for_one, name: ElementTui.Supervisor]
    sup = Supervisor.start_link(children, opts)
    sup
  end
end
