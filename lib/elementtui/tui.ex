defmodule ElementTui.Tui do
  @moduledoc """
  This genserver will be started by the application. It will also handle drawing, in order to make sure we write single threaded.
  """
  use GenServer

  alias ElementTui.TermBox2Ex

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, %{}, opts)
  end

  def puts(x, y, attr, line) do
    GenServer.cast(ElementTui.Tui, {:puts, x, y, attr, line})
  end

  def clear_region(x, y, width, height) do
    GenServer.cast(ElementTui.Tui, {:clear_region, x, y, width, height})
  end

  def present() do
    GenServer.cast(ElementTui.Tui, {:present})
  end

  def input(init, on_enter) do
    GenServer.cast(ElementTui.Tui, {:input, init, on_enter})
  end

  def get_input_buffer() do
    GenServer.call(ElementTui.Tui, :get_input_buffer)
  end

  def poll(timeout_ms) do
    # TODO: we should pass current time, so we account for potential delay in message handling
    GenServer.call(ElementTui.Tui, {:poll, timeout_ms})
  end

  @impl true
  def init(init) do
    TermBox2Ex.ex_start()
    Process.flag(:trap_exit, true)
    {:ok, init}
  end

  @impl true
  def terminate(_reason, _state) do
    TermBox2Ex.ex_stop()
    # IO.puts "Going Down: #{inspect(state)}"
    :normal
  end

  @impl true
  def handle_call(:get_input_buffer, _from, state) do
    {:reply, state[:input][:buffer], state}
  end

  def handle_call({:poll, timeout_ms}, _from, state) do
    ev = TermBox2Ex.poll(timeout_ms)

    require Logger

    case Map.get(state, :input, nil) do
      nil ->
        {:reply, ev, state}

      input ->
        case ev do
          {:event, {1, _, 13, _, _, _, _, _}} ->
            input.on_enter.(input.buffer)
            state = Map.delete(state, :input)
            {:reply, :none, state}

          {:event, {1, _, 127, _, _, _, _, _}} ->
            input = %{input | buffer: String.slice(input.buffer, 0..-2//1)}
            state = Map.put(state, :input, input)
            {:reply, :none, state}

          {:event, {1, _, _, ch, _, _, _, _}} ->
            input = %{input | buffer: input.buffer <> <<ch>>}
            state = Map.put(state, :input, input)
            {:reply, :none, state}

          :none ->
            {:reply, :none, state}

          _ ->
            raise "wrong input"
        end
    end
  end

  @impl true
  def handle_cast({:puts, x, y, attr, line}, state) do
    TermBox2Ex.puts(x, y, attr, line)
    {:noreply, state}
  end

  def handle_cast({:clear_region, x, y, width, height}, state) do
    TermBox2Ex.clear_region(x, y, width, height)
    {:noreply, state}
  end

  def handle_cast({:input, init, on_enter}, state) do
    {:noreply, Map.put(state, :input, %{buffer: init, on_enter: on_enter})}
  end

  def handle_cast({:present}, state) do
    TermBox2Ex.present()
    {:noreply, state}
  end
end
