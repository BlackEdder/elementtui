defmodule ElementTui.TermBox2Ex do
  @moduledoc """
  Thin wrapper around the termbox2 C library. In general a user should not need to use this directly, but instead use the main ElementTui module.
  """

  def so_contents, do: unquote(File.read!("priv/termbox2_nif.so"))

  def load_nifs do
    # NOTE: Traditionally this is done using the priv dir, but we want to support escript.build, which does not support priv.
    # Instead so_contents() contains the binary contents of the ".so" file and we write it to a temp file and load it.
    # TODO: Can we skip the writing to a file and pass the binary directly to :erlang.load_nif?
    {:ok, path} = Briefly.create()
    File.write!(path <> ".so", so_contents())
    :erlang.load_nif(path, 0)
  end

  def ex_start() do
    raise "NIF not implemented"
  end

  def ex_stop() do
    raise "NIF not implemented"
  end

  def poll(_) do
    raise "NIF not implemented"
  end

  def present() do
    raise "NIF not implemented"
  end

  def clear() do
    raise "NIF not implemented"
  end

  def clear_region(_, _, _, _) do
    raise "NIF not implemented"
  end

  def width() do
    raise "NIF not implemented"
  end

  def height() do
    raise "NIF not implemented"
  end

  def puts(_, _, _, _) do
    raise "NIF not implemented"
  end
end
