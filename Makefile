CFLAGS = -g -O3 -pedantic -Wall -Wextra -Wno-unused-parameter

ERLANG_PATH = $(shell erl -eval 'io:format("~s", [lists:concat([code:root_dir(), "/erts-", erlang:system_info(version), "/include"])])' -s init stop -noshell)
CFLAGS += -I$(ERLANG_PATH)
CFLAGS += -Isrc
CFLAGS += -fPIC

LIB_NAME = priv/termbox2_nif.so
.PHONY: all clean

all: $(LIB_NAME)

$(LIB_NAME): src/termbox2_nif.c
	mkdir -p priv
	$(CC) $(CFLAGS) -shared $(LDFLAGS) $^ -o $@

clean:
	rm -f $(LIB_NAME)	
